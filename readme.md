**Members of group task:**
Huebert Miguiel Fabros
Elisabeth Medlien

**The process**
Both of the students had in advance made mental models on how to solve the task.
While completing the task, the students switched turns to programme approximately 
every half hour. Both students engaged in the discussion and reflection that came 
up under the completion of the project. At first, the group used Stack to store 
the opening brackets, where both had the idea of popping each item when the closing 
brackets from the .java file matched. After a lot of back and forth, the students 
agreed to solve the case recursively, and seeing that none of them earlier had used 
recursion before, it would be a great opportunity to test it in practice. The idea 
was to fill an ArrayList with all the brackets from the .java file, and match the 
brackets that were already placed next to each other, and then call the recursive 
function with the updated ArrayList. This allowed the occurrence of a lot of 
exceptions, which lead the group members to learn more about exception handling.

As far as the students know, the solution is working, and they are pleased with 
the teamwork, the gained knowledge, and how the task was completed. 