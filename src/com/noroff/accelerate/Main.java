package com.noroff.accelerate;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) throws IOException {
        // read file here
        String line = "";
        File myFile = new File("Resources/Text.java");
        BufferedReader reader = new BufferedReader(new FileReader(myFile));

        //make ArrayList that will store all relevant characters: {}[]()
        ArrayList<String> characters =new ArrayList<String>();

        while ((line = reader.readLine()) != null) {
            String[] words = line.split("");
            for (String word : words) { //goes through the whole java file and stores the relevant characters in the ArrayList
                if (word.equals("(") || word.equals(")") || word.equals("[") || word.equals("]") || word.equals("{") || word.equals("}")) {
                    characters.add(word);
                }
            }
        }

        //use recursive method to check if the ArrayList contains balanced parenthesis, square brackets and curly brackets
        isMatched(characters);
        System.out.println(characters);

        if(characters.isEmpty()) { //if the ArrayList is empty, then it contained balanced characters, otherwise no.git
            System.out.println("is balanced");
        } else {
            System.out.println("is not balanced");
        }
    }

    private static void isMatched(ArrayList<String> characters) {
        // creating HashMap chars
        HashMap<String, String> chars = new HashMap<String, String>();
        // storing values in HashMap chars
        chars.put("{", "}");
        chars.put("(", ")");
        chars.put("[", "]");

        //traversing through character ArrayList characters
        for (int j = 0; j < characters.size(); j++) {
            //traversing through the hashmap chars
            for (String i : chars.keySet()) {
                // if the arraylist characters contains both the opening bracket AND the closing bracket..
                if (characters.contains(i) && characters.contains(chars.get(i))) {
                    // if f.ex characters["{"] == chars AND the next item characters["}"] == chars["}"]
                    if (characters.get(j).equals(i) && characters.get(j + 1).equals(chars.get(i))) {
                        // remove these two indexes from characters list
                        characters.remove(j + 1);
                        characters.remove(j);
                        // recursion with updated arraylist
                        isMatched(characters);
                    }
                }
            }
        }
    }
}
